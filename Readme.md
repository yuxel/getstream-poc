# Run

    npm install
    NODE_ENV=development nodemon -e js,twig start


# Files that you need to check

    utils.js - which has credentials and server side client initialization
    views/index.twig - twig template file which we do all the actions on frontend with some data passed which read from utils.js
    routes/index.js - which we generate a user token
    routes/feeds.js - which we send a server side message to a feed

