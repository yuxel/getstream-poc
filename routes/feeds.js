var express = require('express');
var router = express.Router();

var utils = require('./../utils');

var config = utils.config;
var client = utils.client;
var feedGroup = utils.feedGroup;
var nickname = utils.nickname;

router.post('/', async function(req, res, next) {
    try {
        var user1 = await client.feed(feedGroup, config.name);
        user1.addActivity({
            actor: nickname,
            tweet: req.body.message,
            verb: 'tweet',
            object: 1
        }).then(function (e) {
            console.log('ok', e);
            res.send("ok");
        }).catch(function (e) {
            console.log('err', e);
            res.send("error");
        });
    } catch (e) {
        console.log(e);
        res.send("error");
    }
});

module.exports = router;
