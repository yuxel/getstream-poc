var express = require('express');
var router = express.Router();

var utils = require('./../utils');

var config = utils.config;
var client = utils.client;
var feedGroup = utils.feedGroup;
var nickname = utils.nickname;

const userToken = client.createUserToken(nickname);

router.get('/', function(req, res, next) {
  res.render('index', { 
      title: 'Express', 
      userToken, 
      nickname, 
      feedConfig: config,
      feedGroup,
  });
});

module.exports = router;
